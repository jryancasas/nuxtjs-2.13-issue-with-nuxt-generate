# NuxtJS 2.13 Issue with nuxt generate

## Steps to reproduce

```bash
# install dependencies
$ yarn install

# build for production (target: server by default)
$ yarn nuxt build

# Try to nuxt generate without rebuilding
$ yarn nuxt generate --no-build --port 7777
```